/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   decryptFile.java
 */

import Crypto.Decryption;
import javax.crypto.*;
import java.io.*;
import java.security.*;

public class decryptFile {
    public static void main(String[] args) throws BadPaddingException, IllegalBlockSizeException, IOException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        String fileIn = args[0];
        String fileOut = args[1];
        String seed = args[2];

        Decryption decryption = Decryption.getInstance().setSeed(seed).decrypt(fileIn);

        if (decryption.writeResult(fileOut)) {
            System.out.printf("%s is decrypted to %s\n", fileIn, fileOut);
        } else {
            System.out.printf("contents of %s were modified\n", fileIn);
        }
    }
}
