/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   secureFile.java
 */

import Crypto.Encryption;
import javax.crypto.*;
import java.io.*;
import java.security.*;

public class secureFile {
    public static void main(String[] args) throws BadPaddingException, IllegalBlockSizeException, IOException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        String fileIn = args[0];
        String fileOut = args[1];
        String seed = args[2];

        Encryption encryption = Encryption.getInstance().setSeed(seed).encrypt(fileIn);

        if (encryption.writeResult(fileOut)) {
            System.out.printf("file %s was encrypted to %s\n", fileIn, fileOut);
        } else {
            System.out.printf("failed to encrypt %s\n", fileIn);
        }
    }
}
